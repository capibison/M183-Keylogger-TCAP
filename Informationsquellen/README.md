# Informationsquellen

## The New York Times
Die New York Times schreibt tendenziell ausführliche Artikeln, gehen jedoch nicht weiter auf technische Funktionsweisen ein bei ihren Artikeln. Es werden vereinzelte Fachbegriffe verwendet, jedoch kaum erläutert was diese genau bedeuten. Nach dem Überfliegen einiger Artikel fällt auf, sie decken jedoch einen grösseren Bereich ab, welcher für den Ottonormalverbraucher verdaulich ist. So decken sie etwa Themen wie Cyberattacken von und auf Nationen ab, Cybersicherheit in Bezug auf Videospiele, Hackingattacken in Bezug auf Kryptotauschbörsen, Hackingattacken bei Wahlen etc. 

## NCSC - Nationales Zentrum fpr Cybersicherheit
Die Artikel der NSCS sind wie man es von der Website der schweizer Regierung erwarten kann sehr sachlich gehalten und für den Durchschnittsbürger verständlich. Jedoch geben sie zu verschiedenen Bereichen auf teils konkrete Hinweise, wie man etwa mit Datensicherung umgehen soll und wie man dies am besten macht. Es sind also nicht einfach blosse Artikel zur Unterhaltung, sondern auch effektive Guides um sein Umgang bezüglich Onlinepräsenz und Sicherheit zu verbessern.

## Swisscom - Cyber Security Report
Der jährliche Cyber Security Report der Swisscom konzentriert sich Jahr um Jahr wieder auf zwei Dinge. Einerseits pflegen sie einen sogenannten Bedrohungsradar. Auf diesem gibt es 6 Bereiche (Dominant Players, Technology Dynamics, Cyber > Physical, Organisation, Physical, Proliferation und Environment/Social) , in welchen sich alle aktuellen Bedorhungen versorgen zu wissen lassen. In diesen werden wie gesagt alle aktuellen grossen Gefahren, Jahr für Jahr untergebracht, je nach deren Bedrohung und es werden deren Bewegungen im Vergleich zum Vorjahr dargestellt. Dazu kommen noch kurze Beschreibungen zu jedem Bereich, was sich dort speziell entwickelt hat, oder was besonders dominant ist bzw was dominant werden könnte in den Folgejahren. Viele dieser Berichte basieren auf Erfahrungsberichten der Swisscom selbst. Zudem gibt es immer noch ein bis zwei Themen pro Jahr, welche ausführlicher beleuchtet und ausgeführt werden, da diese speziell aktuell sind bzw ein speziell grosses Risiko darstellen. Diese Themen wechseln ebenfalls jedes Jahr.

## Ausgewählter Inhalt
https://www.swisscom.ch/content/dam/swisscom/de/about/unternehmen/portraet/netz/sicherheit/documents/security-report-2018.pdf.res/security-report-2018.pdf
In diesem letzten Abschnitt dieses Beitrags nehme ich den Cyber Security Report der Swisscom aus dem Jahre 2018 unter die Lupe.

### Kurzzusammenfassung
Wie oben bereits erwähnt, beinhaltet der Cyber Security Report 2018 wie üblich zwei Teile. Anfangs geht man auf das Lagebild, den Bedrohungsradar ein. Dieser teilt die verschiedenen Bedrohungen in die sechs oben genannten Kategorien ein, und stellt deren Bewegungen im Vergleich zum Vorjahr dar. Im zweiten Teil geht man auf drei Themen ein, welche damals scheinbar sehr für Aufsehen gesorgt haben. Diese sind AI (Artificial Intelligence), Cyber Security, Malware Call Jome und Crypto Mining.
Diese Themen werden Eines um das Andere genauer unter die Lupe genommen, und teils ziemlich ausfürhlich erläutert.

### Einordnung/Reflexion
Der Bericht der Swisscom ist keineswegs ein einfacher Unterhaltungskatalog um von den neusten Unsicherheiten des Internets zu hören, sondern ein sachlich wertvoller Überblick über höchst aktuelle Themen, welche für einen Durchschnittsbürger vermutlich eher uninteressant sein dürften. Die Themen werden teilweise sehr genau beschrieben (so genau wie es auf 20 Seiten eben geht) und können so eine gute Wissensgrundlage zu schaffen, um sich weiter mit den Themen auseinanderzusetzen und sich aktiv darum zu kümmern, wenn man ein Interesse dafür hat.

### Bedeutung für Betriebe
Ich kann mir vorstellen, dass es viele Betriebe in der Schweiz gibt, welche sich auf einen solche Bericht wie Ihn die Swisscom veröffentlicht, beziehen, wenn es zu Sicherheitsverbesserungen in Betrieben kommt. Dies setzt jedoch voraus, dass ein Unternehmen eine grosse Abhängigkeit auf solche Geräte hat und diese eine grosse Rolle spielen, wie zum Beispiel eine Roche, eine UBS, eine FIFA, der Bund uvm. Die Swisscom hat insofern eine grosse Veranwtortung, als dass sie der grösste Internetanbieter der Schweiz ist und dementsprechend, die meisten Daten hat, und die Kapazität diese zu überwachen und auszuwerten und entsprechende Berichte zu veröffentlichen. Insbesondere in der heutigen Zeit, wo alle paar Monate wieder eine neue grosse Bedrohung, potenziell schwerwiegende Bedrohung im Internet auftaucht.

### Auswirkungen auf mein persönliches Verhalten
Obwohl die Swisscom solche Berichte veröffentlicht und gewisse Artikel noch so abschreckend wirken, wird dies (eventuell tragischerweise) bei mir persönlich kaum Auswirkungen haben, dass ich mich anders verhalten werde, da eben doch auch viele Inhalte auch für mich sehr hochstehen sind, obwohl ich ein gewisses Grundlagenwissen besitze. Jedoch gilt es auch zu sagen, dass ich nicht ein KMU oder Grossunternehmen bin, wessen Existenz durch solche topmoderne Angriffe ruiniert werden könnten.